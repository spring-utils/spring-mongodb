package springmongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import springmongodb.resources.Book;

public interface BookRepository extends MongoRepository<Book, String> {

	Book findByTitle(String title);

}
