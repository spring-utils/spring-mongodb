package springmongodb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springmongodb.resources.Book;
import springmongodb.service.BookService;

@RestController
@RequestMapping(BookController.MAPPING)
public class BookController {

	public static final String MAPPING = "/books";
	public static final String TITLE = "/title";
	public static final String PAGES = "/pages";

	@Autowired
	private BookService bookService;

	@GetMapping
	public List<Book> findAll() {
		return bookService.findAll();
	}

	@GetMapping(TITLE)
	public Book get(@PathVariable(TITLE) String title) {
		return bookService.findByTitle(title);
	}

	@PostMapping(TITLE + PAGES)
	public void create(@PathVariable(TITLE) String title, @PathVariable(PAGES) int pages) {
		Book book = bookService.create(title, pages);
		bookService.save(book);
	}

	@DeleteMapping(TITLE)
	public void delete(@PathVariable(TITLE) String title) {
		Book book = bookService.findByTitle(title);
		bookService.delete(book);
	}
}
