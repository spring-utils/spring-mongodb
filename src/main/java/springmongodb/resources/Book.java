package springmongodb.resources;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Book {

	@Id
	public ObjectId id;

	private String title;

	private int pages;
}
