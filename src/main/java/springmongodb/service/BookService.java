package springmongodb.service;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springmongodb.repository.BookRepository;
import springmongodb.resources.Book;

@Service
public class BookService {

	@Autowired
	private BookRepository repository;

	public Book create(String title, int pages) {
		return new Book(ObjectId.get(), title, pages);
	}

	public void save(Book book) {
		repository.save(book);
	}

	public Book findByTitle(String title) {
		return repository.findByTitle(title);
	}

	public List<Book> findAll() {
		return repository.findAll();
	}

	public void delete(Book book) {
		repository.delete(book);
	}
}
