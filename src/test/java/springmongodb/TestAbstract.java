package springmongodb;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.GenericContainer;

@Ignore("Only abstract test class")
@SpringBootTest
@RunWith(SpringRunner.class)
public abstract class TestAbstract {

	// before all other to get database connection
	public static GenericContainer<?> mongoContainer = new GenericContainer<>("mongo:4.2.2").withExposedPorts(27017);

	static {
		mongoContainer.start();

		String propertyHost = "spring.data.mongodb.host";
		String host = mongoContainer.getContainerIpAddress();
		System.setProperty(propertyHost, host);

		String propertyPort = "spring.data.mongodb.port";
		String port = mongoContainer.getFirstMappedPort() + "";
		System.setProperty(propertyPort, port);
	}
}
