package springmongodb.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import springmongodb.TestAbstract;
import springmongodb.resources.Book;

public class BookServiceTest extends TestAbstract {

	@Autowired
	private BookService bookService;

	@Test
	public void save_and_read_books() {
		Book book = bookService.create("Hallo", 12);
		bookService.save(book);

		Book book2 = bookService.findByTitle("Hallo");
		assertThat(book).isEqualTo(book2);

		Book book3 = bookService.create("Hallo2", 25);
		bookService.save(book3);

		assertThat(bookService.findAll()).hasSize(2);
	}
}
