# Login on Container

    mongo -u mongoUser -p mongoPwd --authenticationDatabase mongoDB

# Login via URL

    mongodb://mongoUser:mongoPwd@localhost:27017/?authSource=mongoDB&readPreference=primary&ssl=false

